# PulseAudio Clients to MQTT

I needed a way to know if a PulseAudio server is in use and which
clients are currently connected to it. 


## Usage

```
podman container run -it --rm \
    --env MQTT_HOST=mqtt.custom.zelic.love \
    --env NAME=pa1 \
    --env PULSE_SERVER=10.0.2.61  
    registry.gitlab.com/dejandayoff/paclients2mqtt
```

## Sink-inputs vs Clients
I updated the code to use sink-inputs instead of client list to
determin if it's active or not. 

Shairport-sync will connect and be listed as a client, even if
northing is playing. This results in all the PA servers being used up
prematurly. To fix this, I used sink-list because it will list if a
application is streaming or not. 