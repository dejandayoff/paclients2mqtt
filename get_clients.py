import asyncio
import signal
from contextlib import suppress
import pulsectl_asyncio
import json
import paho.mqtt.client as mqtt
import os
import logging

# Set up logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s %(message)s',
    handlers=[
        logging.StreamHandler(),
    ],
)

print(os.environ.get('PULSE_SERVER'))
NAME = os.environ.get('NAME')
MQTT_HOST = os.environ.get('MQTT_HOST')
MQTT_PORT = int(os.environ.get('MQTT_PORT', '1883'))
MQTT_TOPIC = 'audio/output/{NAME}'.format(NAME=NAME)

async def listen(mqtt_client):
    async with pulsectl_asyncio.PulseAsync('pulseclients2mqtt') as pulse:
        async for event in pulse.subscribe_events('sink_input'):
            status = {
                "active": False,
                "clients": []
            }
            raw_sinks = await pulse.sink_input_list()
            for sink in raw_sinks:
                if not sink.corked:
                    status["active"] = True
                status["clients"].append(sink.__dict__["proplist"])

            # if len(status["clients"]) > 0:
            #     status["active"] = True
            logging.info("{NAME} set to {STATUS} with {NUMOFCLIENTS} clients".format(
                NAME=NAME, 
                STATUS="active" if status["active"] else "not active",
                NUMOFCLIENTS=len(status["clients"])
            ))
            payload = json.dumps(status)
            logging.info(f"Sending {payload} to {MQTT_TOPIC}")
            mqtt_client.publish(MQTT_TOPIC, payload)

async def main():
    # Set up MQTT client
    logging.info("Connecting to MQTT broker at {}:{}".format(MQTT_HOST, MQTT_PORT))
    mqtt_client = mqtt.Client()
    mqtt_client.connect(MQTT_HOST, MQTT_PORT)

    mqtt_client.publish(MQTT_TOPIC, json.dumps({"active": False,"clients": []}))
    # Set up the PulseAudio event loop
    logging.info("Starting PulseAudio event loop")
    listen_task = asyncio.create_task(listen(mqtt_client))

    # Handle signals to cancel the event loop
    for sig in (signal.SIGTERM, signal.SIGHUP, signal.SIGINT):
        loop.add_signal_handler(sig, listen_task.cancel)

    # Run the event loop until the task is cancelled
    with suppress(asyncio.CancelledError):
        await listen_task

    # Disconnect MQTT client
    logging.info("Disconnecting from MQTT broker")
    mqtt_client.disconnect()

# Run event loop until main_task finishes
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
