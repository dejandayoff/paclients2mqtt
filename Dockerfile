FROM python:3.10-slim-buster
LABEL maintainer "Dejan Zelic"

RUN apt-get update && apt-get install -y \
	pulseaudio-utils \
	--no-install-recommends \
	&& rm -rf /var/lib/apt/lists/*

RUN pip install pulsectl-asyncio paho-mqtt 

COPY ./get_clients.py /get_clients.py

ENTRYPOINT [ "python" ]
CMD [ "/get_clients.py" ]
